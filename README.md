# Список покупок
Список покупок с автокомплитом текста с выбором продуктов из локальной бд и сортировкой по категориям продукта.

Приложение написано на Kotlin, используемый стек: MVVM, Room, Coroutines, Dagger 2

##### Скриншоты:
![alt text](https://bitbucket.org/DayzerCode/shoppinglist/raw/4360df986e9c848d06a549603fd3499d22f70c8f/screenshots/v2/lists.png) ![alt text](https://bitbucket.org/DayzerCode/shoppinglist/raw/4360df986e9c848d06a549603fd3499d22f70c8f/screenshots/v2/shopping-list.png)
![alt text](https://bitbucket.org/DayzerCode/shoppinglist/raw/4360df986e9c848d06a549603fd3499d22f70c8f/screenshots/v2/add-to-shopping-list.png) ![alt text](https://bitbucket.org/DayzerCode/shoppinglist/raw/4360df986e9c848d06a549603fd3499d22f70c8f/screenshots/v2/menu.png)