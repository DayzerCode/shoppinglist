package com.d_ershov.shoppinglist.ui.dialogs.add_products_from_prepared_list

import android.os.Bundle
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity

object ProductsFromPreparedListDialogFactory {
    fun create(
        preparedLists: List<PreparedListEntity>,
        currentShoppingListId: Long
    ): ProductsFromPreparedListDialogFragment {
        val dialog = ProductsFromPreparedListDialogFragment(preparedLists)
        val bundle = Bundle()
        bundle.putLong("currentShoppingListId", currentShoppingListId)
        dialog.arguments = bundle
        return dialog
    }
}