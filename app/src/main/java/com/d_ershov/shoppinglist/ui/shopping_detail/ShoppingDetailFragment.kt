package com.d_ershov.shoppinglist.ui.shopping_detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.dto.ShoppingListProductsDto
import com.d_ershov.shoppinglist.db.entities.CategoryEntity
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity
import com.d_ershov.shoppinglist.db.entities.ProductEntity
import com.d_ershov.shoppinglist.states.default_state.StateEnum
import com.d_ershov.shoppinglist.ui.adapters.AutocompleteElementsAdapter
import com.d_ershov.shoppinglist.ui.dialogs.add_new_product.NewProductDialogFactory
import com.d_ershov.shoppinglist.ui.dialogs.add_new_product.NewProductDialogHandler
import com.d_ershov.shoppinglist.ui.dialogs.add_products_from_prepared_list.ProductsFromPreparedListDialogFactory
import com.d_ershov.shoppinglist.ui.dialogs.add_products_from_prepared_list.ProductsFromPreparedListDialogHandler
import com.d_ershov.shoppinglist.ui.shopping_detail.adapters.ChoiceCountAdapter
import com.d_ershov.shoppinglist.ui.shopping_detail.adapters.ShoppingListDetailElementsAdapter
import kotlinx.android.synthetic.main.fragment_shopping_list_detail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ShoppingDetailFragment : Fragment(), NewProductDialogHandler,
    ProductsFromPreparedListDialogHandler {
    val viewModel: ShoppingDetailViewModel by viewModels<ShoppingDetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_list_detail, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(viewModel)
        val bundle = arguments
        if (bundle != null) {
            viewModel.currentShoppingListId = bundle.getLong("shoppingListId", 0)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.state.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                StateEnum.LOADING -> {
                    loadingState()
                    viewModel.loadData()
                }
                StateEnum.READY -> {
                    viewModel.currentShoppingList.observe(viewLifecycleOwner, Observer {
                        tvShoppingListCaption.text = it.name
                    })
                    etSearch.doAfterTextChanged {
                        handleSearchInput(it)
                    }
                    viewModel.showOnlyNotPurchasedProducts.value =
                        cbNotPurchasedProductsToggle.isChecked

                    viewModel.showPanel.observe(viewLifecycleOwner, Observer {
                        togglePanel(it)
                    })

                    viewModel.currentProductEdit.observe(
                        viewLifecycleOwner,
                        Observer { productEntity ->
                            handleChangeCount(productEntity)
                        })

                    cbNotPurchasedProductsToggle.setOnClickListener { view ->
                        viewModel.showOnlyNotPurchasedProducts.value = (view as CheckBox).isChecked
                    }

                    tvFromPreparedList.setOnClickListener {
                        showDialogAddProductsFromPreparedList()
                    }
                    tvShoppingDetailNewProduct.setOnClickListener {
                        if (etSearch.text.isNotEmpty()) {
                            showDialogAddNewProduct(etSearch.text.toString())
                        }
                    }
                    tvShoppingListCaption.setOnClickListener {
                        viewModel.togglePanel()
                    }

                    viewModel.shoppingListProducts.observe(viewLifecycleOwner, Observer {
                        showShoppingListElements(it)
                    })
                    viewModel.productBySearch.observe(viewLifecycleOwner, Observer {
                        showAutocompleteElements(it)
                    })

                    readyState()
                }
                StateEnum.ERROR -> {
                    Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun saveProduct(
        categoryEntity: CategoryEntity,
        newProductName: String
    ) {
        viewModel.saveProductAndAddToList(categoryEntity, newProductName)
    }

    override fun addFromPreparedList(preparedListEntity: PreparedListEntity) {
        viewModel.addProductsFromPreparedList(preparedListEntity)
    }

    private fun togglePanel(it: Boolean?) {
        val needShow = it ?: false
        var icon = R.drawable.ic_arrow_drop_up_green_24dp
        if (needShow) {
            llPanelContent.visibility = View.VISIBLE
        } else {
            llPanelContent.visibility = View.GONE
            icon = R.drawable.ic_arrow_drop_down_green_24dp
        }
        tvShoppingListCaption.setCompoundDrawablesWithIntrinsicBounds(
            0,
            0,
            icon,
            0
        )
    }

    private fun showShoppingListElements(list: List<ShoppingListProductsDto>) {
        if (list.isNotEmpty()) {
            tvShoppingDetailEmptyText.visibility = View.GONE
            rvShoppingListElements.visibility = View.VISIBLE
            if (rvShoppingListElements.adapter == null || rvShoppingListElements.adapter?.itemCount ?: 0 < list.size) {
                val adapter = ShoppingListDetailElementsAdapter(list as MutableList<ShoppingListProductsDto>)
                rvShoppingListElements.adapter = adapter
                adapter.onDeleteElement = { element ->
                    viewModel.deleteElementFromShoppingList(element)
                }
                adapter.onSelectElement = { element, isChecked ->
                    element.bought = isChecked
                    viewModel.updateElementFromShoppingList(element)
                }
                adapter.onChangeCount = { productId ->
                    viewModel.setCurrentProductEdit(productId)
                }
            }
        } else {
            tvShoppingDetailEmptyText.visibility = View.VISIBLE
            rvShoppingListElements.visibility = View.GONE
        }
    }

    private fun showAutocompleteElements(list: List<ProductEntity>) {
        if (list.isNotEmpty()) {
            showAutoCompleteBlock()
            val adapter =
                AutocompleteElementsAdapter(
                    list,
                    viewModel.getShoppingListProductsIdArray()
                )
            rvAutocompleteElements.adapter = adapter
            adapter.onAddElement = { product ->
                if (product.productId != null) {
                    viewModel.addProductInShoppingList(product.productId!!)
                    viewModel.increaseProductCounterNumberAdditions(product)
                    etSearch.setText("")
                    viewModel.currentProductEdit.value = product
                }
            }
        } else {
            hideAllBlocks()
        }
    }

    private fun loadingState() {
        llShoppingDetailLoading.visibility = View.VISIBLE
        llShoppingDetailReady.visibility = View.GONE
    }

    private fun readyState() {
        llShoppingDetailLoading.visibility = View.GONE
        llShoppingDetailReady.visibility = View.VISIBLE
    }

    private fun showDialogAddNewProduct(newProductName: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val categories = viewModel.getAllCategories()
            withContext(Dispatchers.Main) {
                val dialog = NewProductDialogFactory.create(
                    categories,
                    newProductName,
                    viewModel.currentShoppingListId
                )
                dialog.show(childFragmentManager, "AddNewProductDialogFragment")
            }
        }
    }

    private fun showDialogAddProductsFromPreparedList() {
        CoroutineScope(Dispatchers.IO).launch {
            val preparedLists = viewModel.getPreparedLists()
            withContext(Dispatchers.Main) {
                val dialog: DialogFragment = ProductsFromPreparedListDialogFactory.create(
                    preparedLists,
                    viewModel.currentShoppingListId
                )
                dialog.show(childFragmentManager, "AddProductsFromPreparedListDialogFragment")
            }
        }
    }

    private fun handleSearchInput(edit: Editable?) {
        if (edit.toString().isNotEmpty()) {
            viewModel.runSearchProductByName(edit.toString() + "%")
            tvShoppingDetailNewProduct.visibility = View.VISIBLE
        } else {
            rvAutocompleteElements.visibility = View.GONE
            tvShoppingDetailNewProduct.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun handleChangeCount(productEntity: ProductEntity?) {
        if (productEntity !== null) {
            tvChoiceLabel.text =
                productEntity.name + resources.getString(R.string.fragment_shopping_list_detail_change_count)
            val adapter = ChoiceCountAdapter(productEntity)
            rvChoiceCount.adapter = adapter
            adapter.onSelectElement = { productId, count ->
                viewModel.updateProductInShoppingList(productId, count)
                viewModel.currentProductEdit.value = null
            }
            showChoiceCountBlock()
        } else {
            hideAllBlocks()
        }
    }

    private fun showAutoCompleteBlock() {
        rvAutocompleteElements.visibility = View.VISIBLE
        llChoiceBlock.visibility = View.GONE
    }

    private fun showChoiceCountBlock() {
        rvAutocompleteElements.visibility = View.GONE
        llChoiceBlock.visibility = View.VISIBLE
    }

    private fun hideAllBlocks() {
        rvAutocompleteElements.visibility = View.GONE
        llChoiceBlock.visibility = View.GONE
    }
}
