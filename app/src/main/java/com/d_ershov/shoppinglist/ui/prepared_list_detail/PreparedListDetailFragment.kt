package com.d_ershov.shoppinglist.ui.prepared_list_detail

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.states.default_state.StateEnum
import com.d_ershov.shoppinglist.ui.adapters.AutocompleteElementsAdapter
import com.d_ershov.shoppinglist.ui.prepared_list_detail.adapters.PreparedListDetailElementsAdapter
import kotlinx.android.synthetic.main.fragment_prepared_list_detail.*
import kotlinx.android.synthetic.main.fragment_shopping_list_detail.etSearch
import kotlinx.android.synthetic.main.fragment_shopping_list_detail.rvAutocompleteElements

class PreparedListDetailFragment : Fragment() {
    val viewModel: PreparedListDetailViewModel by viewModels<PreparedListDetailViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_prepared_list_detail, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        App.appComponent.inject(viewModel)
        val bundle = arguments
        if (bundle != null) {
            val receivedId = bundle.getLong("preparedListId", 0)
            viewModel.currentPreparedListId = receivedId
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.state.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                StateEnum.LOADING -> {
                    loadingState()
                    viewModel.loadData()
                }
                StateEnum.READY -> {
                    viewModel.currentPreparedList.observe(viewLifecycleOwner, Observer {

                        etNameList.setText(it.name)
                        etNameList.setSelectAllOnFocus(true)
                    })

                    etNameList.setOnFocusChangeListener { view: View, _: Boolean ->
                        val newListName = (view as android.widget.EditText).text.toString()
                        viewModel.renameList(newListName)
                    }

                    etSearch.doAfterTextChanged {
                        handleSearchInput(it)
                    }

                    showPreparedListElements()
                    showAutocompleteElements()
                    readyState()
                }
                StateEnum.ERROR -> {
                    Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun handleSearchInput(edit: Editable?) {
        if (edit.toString().isNotEmpty()) {
            viewModel.runSearchProductByName(edit.toString() + "%")
        } else {
            rvAutocompleteElements.visibility = View.GONE
        }
    }

    private fun showPreparedListElements() {
        viewModel.preparedListProducts.observe(viewLifecycleOwner, Observer {
            if (viewModel.preparedListProducts.value != null && viewModel.preparedListProducts.value!!.isNotEmpty()) {
                val adapter = PreparedListDetailElementsAdapter(it)
                rvPreparedListElements.adapter = adapter
                adapter.onDeleteElement = {
                    product ->
                        viewModel.deleteElementFromPreparedList(product.productId)
                }
                rvPreparedListElements.visibility = View.VISIBLE
                tvPreparedListElementsEmptyText.visibility = View.GONE
            } else {
                rvPreparedListElements.visibility = View.GONE
                tvPreparedListElementsEmptyText.visibility = View.VISIBLE
            }
        })
    }

    private fun showAutocompleteElements() {
        viewModel.productBySearch.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                rvAutocompleteElements.visibility = View.VISIBLE
                val adapter = AutocompleteElementsAdapter(it, viewModel.getShoppingListProductsIdArray())
                rvAutocompleteElements.adapter = adapter
                adapter.onAddElement = { product ->
                    if (product.productId != null) {
                        viewModel.addProductInPreparedList(product.productId!!)
                        etSearch.setText("")
                    }
                }
            } else {
                rvAutocompleteElements.visibility = View.GONE
            }
        })
    }


    private fun loadingState() {
        llLoading.visibility = View.VISIBLE
        llReadyState.visibility = View.GONE
    }

    private fun readyState() {
        llLoading.visibility = View.GONE
        llReadyState.visibility = View.VISIBLE
    }
}
