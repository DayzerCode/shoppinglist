package com.d_ershov.shoppinglist.ui.rating.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.ProductEntity

class RatingProductsAdapter(
    private var list: List<ProductEntity>
) : RecyclerView.Adapter<RatingProductsViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RatingProductsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.rating_element, parent, false)
        return RatingProductsViewHolder(view)
    }

    override fun onBindViewHolder(holderProducts: RatingProductsViewHolder, position: Int) {
        holderProducts.tvName.text = list[position].name
        holderProducts.tvNumberAdditions.text = list[position].numberAdditions.toString()
    }

    override fun getItemCount(): Int {
        return list.size
    }
}