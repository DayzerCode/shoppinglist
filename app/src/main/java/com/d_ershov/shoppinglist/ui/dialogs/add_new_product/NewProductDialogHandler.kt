package com.d_ershov.shoppinglist.ui.dialogs.add_new_product

import com.d_ershov.shoppinglist.db.entities.CategoryEntity

interface NewProductDialogHandler {
    fun saveProduct(categoryEntity: CategoryEntity, newProductName: String)
}