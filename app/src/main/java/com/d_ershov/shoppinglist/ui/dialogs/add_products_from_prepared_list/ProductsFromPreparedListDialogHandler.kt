package com.d_ershov.shoppinglist.ui.dialogs.add_products_from_prepared_list

import com.d_ershov.shoppinglist.db.entities.PreparedListEntity

interface ProductsFromPreparedListDialogHandler {
    fun addFromPreparedList(preparedListEntity: PreparedListEntity)
}