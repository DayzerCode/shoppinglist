package com.d_ershov.shoppinglist.ui.rating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.ProductEntity
import com.d_ershov.shoppinglist.ui.rating.adapters.RatingProductsAdapter
import kotlinx.android.synthetic.main.fragment_prepared_lists.llEmptyState
import kotlinx.android.synthetic.main.fragment_rating.*


class RatingFragment : Fragment() {
    val viewModel: RatingViewModel by viewModels<RatingViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_rating, container, false)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.lists.observe(viewLifecycleOwner, Observer {
            showRatingElements(it)
        })

    }

    private fun showRatingElements(list: List<ProductEntity>) {
        if (list.isNotEmpty()) {
            val adapter = RatingProductsAdapter(list)
            rvRatingList.adapter = adapter

            llReadyState.visibility = View.VISIBLE
            llEmptyState.visibility = View.GONE
        } else {
            llReadyState.visibility = View.GONE
            llEmptyState.visibility = View.VISIBLE
        }
    }
}
