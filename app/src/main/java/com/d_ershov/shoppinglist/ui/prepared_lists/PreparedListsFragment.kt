package com.d_ershov.shoppinglist.ui.prepared_lists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.ui.prepared_lists.adapters.PreparedListsAdapter
import kotlinx.android.synthetic.main.fragment_prepared_lists.*
import kotlinx.android.synthetic.main.fragment_shopping_lists.rvShoppingLists


class PreparedListsFragment : Fragment() {
    val viewModel: PreparedListsViewModel by viewModels<PreparedListsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_prepared_lists, container, false)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvNewList.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_green_24dp, 0,0,0)
        showPreparedListElements()
        tvNewList.setOnClickListener {
            navigateToElement(null)
        }
    }

    private fun showPreparedListElements() {
        viewModel.lists.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val adapter = PreparedListsAdapter(it)
                rvShoppingLists.adapter = adapter

                adapter.onDelete = { list ->
                    viewModel.deleteList(list.preparedListId!!)
                }
                adapter.onNavigateToDetail = { list -> navigateToElement(list.preparedListId!!)}
                rvShoppingLists.visibility = View.VISIBLE
                llEmptyState.visibility = View.GONE
            } else {
                rvShoppingLists.visibility = View.GONE
                llEmptyState.visibility = View.VISIBLE
            }
        })
    }

    private fun navigateToElement(id: Long?) {
        val bundle = Bundle()
        if (id != null) {
            bundle.putLong("preparedListId", id)
        }
        findNavController().navigate(R.id.nav_prepared_list_detail, bundle)
    }
}
