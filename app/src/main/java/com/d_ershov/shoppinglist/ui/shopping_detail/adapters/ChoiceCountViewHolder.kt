package com.d_ershov.shoppinglist.ui.shopping_detail.adapters

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class ChoiceCountViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val llChoiceCount: LinearLayout = view.findViewById(R.id.llChoiceCount)
    val tvVariantCount: TextView = view.findViewById(R.id.tvVariantCount)
}