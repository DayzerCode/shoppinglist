package com.d_ershov.shoppinglist.ui.dialogs.add_new_product

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.CategoryEntity

open class NewProductDialogFragment(private val categories: List<CategoryEntity>) : DialogFragment() {
    private lateinit var adapter: ArrayAdapter<CategoryEntity>
    private lateinit var dialogHandler: NewProductDialogHandler
    private var newProductName: String? = null
    private var currentShoppingListId: Long? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogHandler = parentFragment as NewProductDialogHandler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        val bundle = requireArguments()
        newProductName = bundle.getString("newProductName")
        currentShoppingListId = bundle.getLong("currentShoppingListId")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        adapter =  getCategoryArrayAdapter(categories)
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setTitle(resources.getString(R.string.add) + " " + newProductName)
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .setAdapter(adapter) { _, position ->
                    if (newProductName != null) {
                        dialogHandler.saveProduct(categories[position], newProductName!!)
                    }
            }
        return builder.create()
    }

    fun getCategoryArrayAdapter(categories: List<CategoryEntity>): ArrayAdapter<CategoryEntity> {
        val adapter = ArrayAdapter<CategoryEntity>(requireContext(), android.R.layout.select_dialog_item)
        adapter.addAll(categories)
        return adapter
    }
}