package com.d_ershov.shoppinglist.ui.shopping_detail.adapters

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class ShoppingListDetailElementsViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val tvShoppingListElement: TextView = view.findViewById(R.id.tvShoppingListElement)
    val tvShoppingListElementCount: TextView = view.findViewById(R.id.tvShoppingListElementCount)
    val tvClickChangeCount: TextView = view.findViewById(R.id.tvClickChangeCount)
    val tvCategory: TextView = view.findViewById(R.id.tvCategory)
    val ivShoppingListElementDelete: ImageView = view.findViewById(R.id.ivShoppingListElementDelete)
    val rbShoppingDetailElementBought: CheckBox = view.findViewById(R.id.rbShoppingDetailElementBought)
}