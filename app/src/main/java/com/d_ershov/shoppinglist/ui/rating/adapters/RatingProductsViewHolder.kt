package com.d_ershov.shoppinglist.ui.rating.adapters

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class RatingProductsViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val tvName: TextView = view.findViewById(R.id.tvName)
    val tvNumberAdditions: TextView = view.findViewById(R.id.tvNumberAdditions)
}