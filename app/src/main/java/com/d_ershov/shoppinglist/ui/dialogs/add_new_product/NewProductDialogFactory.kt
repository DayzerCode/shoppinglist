package com.d_ershov.shoppinglist.ui.dialogs.add_new_product

import android.os.Bundle
import com.d_ershov.shoppinglist.db.entities.CategoryEntity

object NewProductDialogFactory {
    fun create(categories: List<CategoryEntity>, newProductName: String, currentShoppingListId: Long) : NewProductDialogFragment {
        val dialog = NewProductDialogFragment(categories)
        val bundle = Bundle()
        bundle.putString("newProductName", newProductName)
        bundle.putLong("currentShoppingListId", currentShoppingListId)
        dialog.arguments = bundle
        return dialog
    }
}