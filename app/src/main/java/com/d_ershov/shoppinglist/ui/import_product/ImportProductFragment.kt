package com.d_ershov.shoppinglist.ui.import_product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.states.state_import.StateImport
import kotlinx.android.synthetic.main.fragment_import_product.*

class ImportProductFragment : Fragment() {
    val viewModel: ImportProductViewModel by viewModels<ImportProductViewModel>()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_import_product, container, false)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.loadState()
        viewModel.state.observe(viewLifecycleOwner, Observer {
            when (it) {
                StateImport.success -> {
                    showSuccess()
                }
                StateImport.noProducts -> {
                    showNoProducts()
                }
            }
        })
    }

    private fun showSuccess() {
        llImportProductNoProducts.visibility = View.GONE
        llImportProductUpdatedSuccess.visibility = View.VISIBLE
        llImportProductLoading.visibility = View.GONE
    }

    private fun showNoProducts() {
        llImportProductNoProducts.visibility = View.VISIBLE
        llImportProductUpdatedSuccess.visibility = View.GONE
        llImportProductLoading.visibility = View.GONE

        btnAddProductsInDb.setOnClickListener {
            viewModel.import()
        }
    }
}
