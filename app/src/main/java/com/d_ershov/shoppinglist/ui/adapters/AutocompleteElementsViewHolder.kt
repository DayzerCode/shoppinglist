package com.d_ershov.shoppinglist.ui.adapters

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class AutocompleteElementsViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val llAutocompleteElement: LinearLayout = view.findViewById(R.id.llAutocompleteElement)
    val tvAutocompleteElementText: TextView = view.findViewById(R.id.tvAutocompleteElementText)
    val tvAutocompleteElementCanAddStatus: TextView = view.findViewById(R.id.tvAutocompleteElementCanAddStatus)
    val ivAutocompleteElementAddedStatus: ImageView = view.findViewById(R.id.ivAutocompleteElementAddedStatus)
}