package com.d_ershov.shoppinglist.ui.prepared_list_detail.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class PreparedListDetailElementsViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val tvName: TextView = view.findViewById(R.id.tvName)
    val ivDelete: ImageView = view.findViewById(R.id.ivDelete)
}