package com.d_ershov.shoppinglist.ui.dialogs.add_products_from_prepared_list

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity

open class ProductsFromPreparedListDialogFragment(
    private val preparedLists: List<PreparedListEntity>
) : DialogFragment() {
    private lateinit var adapter: ArrayAdapter<PreparedListEntity>
    private lateinit var dialogHandler: ProductsFromPreparedListDialogHandler
    private var currentShoppingListId: Long? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogHandler = parentFragment as ProductsFromPreparedListDialogHandler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        val bundle = requireArguments()
        currentShoppingListId = bundle.getLong("currentShoppingListId")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        adapter = getCategoryArrayAdapter(preparedLists)
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setTitle(resources.getString(R.string.fragment_shopping_list_detail_from_prepared_list))
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .setAdapter(adapter) { _, position ->
                dialogHandler.addFromPreparedList(preparedLists[position])
            }
        return builder.create()
    }

    private fun getCategoryArrayAdapter(categories: List<PreparedListEntity>): ArrayAdapter<PreparedListEntity> {
        val adapter =
            ArrayAdapter<PreparedListEntity>(requireContext(), android.R.layout.select_dialog_item)
        adapter.addAll(categories)
        return adapter
    }
}