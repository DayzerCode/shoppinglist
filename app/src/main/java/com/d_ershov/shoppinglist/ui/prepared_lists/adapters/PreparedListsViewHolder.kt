package com.d_ershov.shoppinglist.ui.prepared_lists.adapters

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class PreparedListsViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val llPreparedDetailElement: LinearLayout = view.findViewById(R.id.llPreparedDetailElement)
    val tvName: TextView = view.findViewById(R.id.tvName)
    val ivDelete: ImageView = view.findViewById(R.id.ivDelete)
}