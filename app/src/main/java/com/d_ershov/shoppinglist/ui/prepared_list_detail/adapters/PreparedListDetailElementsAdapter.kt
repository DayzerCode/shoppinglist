package com.d_ershov.shoppinglist.ui.prepared_list_detail.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.dto.PreparedListProductsDto

class PreparedListDetailElementsAdapter(
    private var products: List<PreparedListProductsDto>
) : RecyclerView.Adapter<PreparedListDetailElementsViewHolder>() {
    var onDeleteElement: ((PreparedListProductsDto) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PreparedListDetailElementsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.prepared_list_detail_element, parent, false)
        return PreparedListDetailElementsViewHolder(view)
    }

    override fun onBindViewHolder(holder: PreparedListDetailElementsViewHolder, position: Int) {
        holder.tvName.text = products[position].productName

        holder.ivDelete.setOnClickListener {
            onDeleteElement?.invoke(products[position])
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }
}