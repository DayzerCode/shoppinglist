package com.d_ershov.shoppinglist.ui.prepared_lists

import androidx.lifecycle.ViewModel
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.repositories.PreparedListProductsRepository
import com.d_ershov.shoppinglist.repositories.PreparedListRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class PreparedListsViewModel() : ViewModel() {
    @Inject
    lateinit var preparedListRepository: PreparedListRepository
    @Inject
    lateinit var preparedListProductsRepository: PreparedListProductsRepository

    init {
        App.appComponent.inject(this)
    }

    val lists = preparedListRepository.findAll()
    fun deleteList(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            preparedListProductsRepository.deleteByPreparedListId(id)
            preparedListRepository.delete(id)
        }
    }
}