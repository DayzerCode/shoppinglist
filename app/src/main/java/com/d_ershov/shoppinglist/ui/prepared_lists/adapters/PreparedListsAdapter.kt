package com.d_ershov.shoppinglist.ui.prepared_lists.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity

class PreparedListsAdapter(
    private var list: List<PreparedListEntity>
) : RecyclerView.Adapter<PreparedListsViewHolder>() {
    var onDelete : ((PreparedListEntity) -> Unit)? = null
    var onNavigateToDetail : ((PreparedListEntity) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PreparedListsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.prepared_lists, parent, false)
        return PreparedListsViewHolder(view)
    }

    override fun onBindViewHolder(holder: PreparedListsViewHolder, position: Int) {
        holder.tvName.text = list[position].name
        holder.ivDelete.setOnClickListener {
            onDelete?.invoke(list[position])
        }

        holder.llPreparedDetailElement.setOnClickListener {
            onNavigateToDetail?.invoke(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}