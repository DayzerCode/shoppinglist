package com.d_ershov.shoppinglist.ui.shopping_lists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.ui.shopping_lists.adapters.ShoppingListsAdapter
import kotlinx.android.synthetic.main.fragment_shopping_lists.*


class ShoppingListsFragment : Fragment() {
    val viewModel: ShoppingListsViewModel by viewModels<ShoppingListsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_shopping_lists, container, false)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showShoppingListElements()
    }

    private fun showShoppingListElements() {
        viewModel.getShoppingLists().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val adapter = ShoppingListsAdapter(it)
                rvShoppingLists.adapter = adapter

                adapter.onDelete = { shoppingList ->
                    viewModel.deleteShoppingList(shoppingList.shoppingListId)
                }

                adapter.onNavigateToDetail = { shoppingList ->
                    val bundle = Bundle()
                    bundle.putLong("shoppingListId", shoppingList.shoppingListId)
                    findNavController().navigate(R.id.nav_add_list, bundle)
                }
            }
            readyState()
        })
    }

    private fun readyState() {
        llShoppingLists.visibility = View.VISIBLE
    }
}
