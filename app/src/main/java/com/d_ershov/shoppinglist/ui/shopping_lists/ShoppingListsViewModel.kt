package com.d_ershov.shoppinglist.ui.shopping_lists

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.d_ershov.shoppinglist.db.dto.ShoppingListWithNumberProductsDto
import com.d_ershov.shoppinglist.repositories.ShoppingListRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ShoppingListsViewModel() : ViewModel() {
    @Inject
    lateinit var repository: ShoppingListRepository

    fun getShoppingLists() : LiveData<List<ShoppingListWithNumberProductsDto>> {
        return repository.getShoppingListsWithNumberProducts()
    }
    fun deleteShoppingList(shoppingListId: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.delete(shoppingListId)
        }
    }
}