package com.d_ershov.shoppinglist.ui.shopping_lists.adapters

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R

class ShoppingListsViewHolder(view: View) :  RecyclerView.ViewHolder(view) {
    val llShoppingListElement: LinearLayout = view.findViewById(R.id.llShoppingListElement)
    val tvName: TextView = view.findViewById(R.id.tvName)
    val ivDelete: ImageView = view.findViewById(R.id.ivDelete)
    val tvNumberPurchasedItems: TextView = view.findViewById(R.id.tvNumberPurchasedItems)
    val tvNumberItems: TextView = view.findViewById(R.id.tvNumberItems)
}