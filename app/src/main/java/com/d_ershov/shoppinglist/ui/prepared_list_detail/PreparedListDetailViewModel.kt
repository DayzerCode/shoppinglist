package com.d_ershov.shoppinglist.ui.prepared_list_detail

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.dto.PreparedListProductsDto
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity
import com.d_ershov.shoppinglist.db.entities.ProductEntity
import com.d_ershov.shoppinglist.repositories.PreparedListProductsRepository
import com.d_ershov.shoppinglist.repositories.PreparedListRepository
import com.d_ershov.shoppinglist.repositories.ProductsRepository
import com.d_ershov.shoppinglist.states.default_state.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PreparedListDetailViewModel() : ViewModel() {
    @Inject
    lateinit var preparedListRepository: PreparedListRepository
    @Inject
    lateinit var preparedListProductsRepository: PreparedListProductsRepository
    @Inject
    lateinit var productsRepository: ProductsRepository
    @Inject
    lateinit var resources: Resources

    var currentPreparedListId: Long = 0
    lateinit var currentPreparedList: LiveData<PreparedListEntity>
    lateinit var preparedListProducts: LiveData<List<PreparedListProductsDto>>
    val productBySearch = MutableLiveData<List<ProductEntity>>()
    val state = MutableLiveData(State.loading)

    init {
        App.appComponent.inject(this)
    }
    fun loadData() {
        CoroutineScope(Dispatchers.IO).launch {
            if (currentPreparedListId.equals(0L)) {
                currentPreparedListId =
                    preparedListRepository.new(resources.getString(R.string.fragment_prepared_list_detail_new))
            }
            currentPreparedList = preparedListRepository.findById(currentPreparedListId)
            preparedListProducts =
                preparedListProductsRepository.findProductsByPreparedList(currentPreparedListId)
            withContext(Dispatchers.Main) {
                state.value = State.ready
            }
        }
    }

    fun deleteElementFromPreparedList(productId: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            preparedListProductsRepository.delete(currentPreparedListId, productId)
        }
    }

    fun runSearchProductByName(query: String) {
        viewModelScope.launch {
            productBySearch.value = productsRepository.searchByName(query)
        }
    }

    fun addProductInPreparedList(productId: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            preparedListProductsRepository.add(currentPreparedListId, productId)
        }
    }

    fun getShoppingListProductsIdArray(): ArrayList<Long> {
        val arrayList = ArrayList<Long>()
        if (!preparedListProducts.value.isNullOrEmpty()) {
            for (element in preparedListProducts.value!!)  {
                arrayList.add(element.productId)
            }
        }
        return arrayList
    }

    fun renameList(newListName: String) {
        if (currentPreparedList.value != null && currentPreparedList.value!!.name !=  newListName) {
            CoroutineScope(Dispatchers.IO).launch {
                currentPreparedList.value!!.name = newListName
                preparedListRepository.update(currentPreparedList.value!!)
            }
        }
    }
}