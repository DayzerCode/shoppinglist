package com.d_ershov.shoppinglist.ui.shopping_detail.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.ProductEntity

class ChoiceCountAdapter(private val currentProductEntity: ProductEntity) : RecyclerView.Adapter<ChoiceCountViewHolder>() {
    var onSelectElement: ((Long, Short) -> Unit)? = null
    val MAX_COUNT = 20
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChoiceCountViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.choice_count, parent, false)
        return ChoiceCountViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ChoiceCountViewHolder, position: Int) {
        val currentCount = position + 1
        holder.tvVariantCount.text = currentCount.toString()

        holder.llChoiceCount.setOnClickListener {
            onSelectElement?.invoke(currentProductEntity.productId!!, currentCount.toShort())
        }
    }

    override fun getItemCount(): Int {
        return MAX_COUNT
    }
}