package com.d_ershov.shoppinglist.ui.shopping_detail

import android.content.res.Resources
import androidx.lifecycle.*
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.dto.ShoppingListProductsDto
import com.d_ershov.shoppinglist.db.entities.CategoryEntity
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity
import com.d_ershov.shoppinglist.db.entities.ProductEntity
import com.d_ershov.shoppinglist.db.entities.ShoppingListEntity
import com.d_ershov.shoppinglist.extensions.getCurrentAsString
import com.d_ershov.shoppinglist.repositories.*
import com.d_ershov.shoppinglist.states.default_state.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ShoppingDetailViewModel() : ViewModel() {
    @Inject
    lateinit var productsRepository: ProductsRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Inject
    lateinit var shoppingListRepository: ShoppingListRepository

    @Inject
    lateinit var shoppingListProductsRepository: ShoppingListProductsRepository

    @Inject
    lateinit var preparedListRepository: PreparedListRepository

    @Inject
    lateinit var preparedListProductsRepository: PreparedListProductsRepository

    @Inject
    lateinit var resources: Resources

    var currentShoppingListId: Long = 0
    lateinit var currentShoppingList: LiveData<ShoppingListEntity>
    lateinit var shoppingListProducts: LiveData<List<ShoppingListProductsDto>>
    val productBySearch = MutableLiveData<List<ProductEntity>>()
    val showOnlyNotPurchasedProducts = MutableLiveData(false)
    val showPanel = MutableLiveData(false)
    val state = MutableLiveData(State.loading)
    val currentProductEdit = MutableLiveData<ProductEntity>()

    fun loadData() {
        CoroutineScope(Dispatchers.IO).launch {
            if (currentShoppingListId.equals(0L)) {
                val currentDateString = Date().getCurrentAsString()
                currentShoppingListId =
                    shoppingListRepository.new(resources.getString(R.string.fragment_shopping_list_detail_new_list) + " " + currentDateString)
            }
            currentShoppingList = shoppingListRepository.getById(currentShoppingListId)
            shoppingListProducts = Transformations.switchMap(showOnlyNotPurchasedProducts) {
                shoppingListProductsRepository.getProducts(
                    currentShoppingListId,
                    showOnlyNotPurchasedProducts.value!!
                )
            } as MutableLiveData<List<ShoppingListProductsDto>>
            withContext(Dispatchers.Main) {
                state.value = State.ready
            }
        }
    }

    fun deleteElementFromShoppingList(element: ShoppingListProductsDto) {
        CoroutineScope(Dispatchers.IO).launch {
            shoppingListProductsRepository.delete(element.productId, element.shoppingListId)
        }
    }

    fun updateElementFromShoppingList(element: ShoppingListProductsDto) {
        CoroutineScope(Dispatchers.IO).launch {
            shoppingListProductsRepository.update(element)
        }
    }

    fun runSearchProductByName(query: String) {
        showOnlyNotPurchasedProducts.value = false
        viewModelScope.launch {
            productBySearch.value = productsRepository.searchByName(query)
        }
    }

    fun addProductInShoppingList(productId: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            var alreadyAdded = false
            shoppingListProducts.value.let {
                if (it != null && it.isNotEmpty()) {
                    for (productInShoppingList in it) {
                        if (productInShoppingList.productId == productId) {
                            alreadyAdded = true
                        }
                    }
                }
            }
            if (!alreadyAdded) {
                shoppingListProductsRepository.addInShoppingList(productId, currentShoppingListId)
            }
        }
    }

    fun increaseProductCounterNumberAdditions(product: ProductEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            productsRepository.increaseCounterAdditions(product)
        }
    }

    fun saveProductAndAddToList(categoryEntity: CategoryEntity, newProductName: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val newProductId =
                productsRepository.add(ProductEntity(newProductName, categoryEntity.categoryId!!))
            shoppingListProductsRepository.addInShoppingList(newProductId, currentShoppingListId)
        }
    }

    fun addProductsFromPreparedList(preparedList: PreparedListEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            if (preparedList.preparedListId != null) {
                val productsList =
                    preparedListProductsRepository.getProductsByPreparedList(preparedList.preparedListId!!)
                if (productsList.isNotEmpty()) {
                    for (product in productsList) {
                        addProductInShoppingList(product.productId)
                    }
                }
            }
        }
    }

    suspend fun getAllCategories(): List<CategoryEntity> {
        return categoryRepository.getAll()
    }

    fun getShoppingListProductsIdArray(): ArrayList<Long> {
        val arrayList = ArrayList<Long>()
        if (!shoppingListProducts.value.isNullOrEmpty()) {
            for (element in shoppingListProducts.value!!) {
                arrayList.add(element.productId)
            }
        }
        return arrayList
    }

    suspend fun getPreparedLists(): List<PreparedListEntity> {
        return preparedListRepository.getAll()
    }

    fun togglePanel() {
        showPanel.value = !showPanel.value!!
    }

    fun updateProductInShoppingList(productId: Long, count: Short) {
        CoroutineScope(Dispatchers.IO).launch {
            shoppingListProductsRepository.updateCount(productId, currentShoppingListId, count)
        }
    }

    fun setCurrentProductEdit(productId: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            val productEntity = productsRepository.getById(productId)
            if (productEntity != null) {
                withContext(Dispatchers.Main) {
                    currentProductEdit.value = productEntity
                }
            }
        }
    }
}