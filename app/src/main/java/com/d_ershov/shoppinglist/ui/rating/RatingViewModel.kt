package com.d_ershov.shoppinglist.ui.rating

import androidx.lifecycle.ViewModel
import com.d_ershov.shoppinglist.App
import com.d_ershov.shoppinglist.repositories.ProductsRepository
import javax.inject.Inject

class RatingViewModel() : ViewModel() {
    @Inject
    lateinit var productsRepository: ProductsRepository

    init {
        App.appComponent.inject(this)
    }

    val lists = productsRepository.findAllForRating()
}