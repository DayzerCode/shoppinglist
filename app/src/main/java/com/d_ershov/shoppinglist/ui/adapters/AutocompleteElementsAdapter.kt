package com.d_ershov.shoppinglist.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.entities.ProductEntity

class AutocompleteElementsAdapter(
    private var products: List<ProductEntity>,
    private var currentShoppingListProducts: ArrayList<Long>
) : RecyclerView.Adapter<AutocompleteElementsViewHolder>() {
    var onAddElement: ((ProductEntity) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AutocompleteElementsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.autocomplete_element, parent, false)
        return AutocompleteElementsViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: AutocompleteElementsViewHolder, position: Int) {
        holder.tvAutocompleteElementText.text = products[position].name
        var isProductAlreadyAdded = false
        for (productIdInShoppingList in currentShoppingListProducts) {
            if (productIdInShoppingList == products[position].productId) {
                isProductAlreadyAdded = true
                break
            }
        }
        if (isProductAlreadyAdded) {
            productAlreadyAdded(holder)
        } else {
            productCanBeAdded(holder, position)
        }
    }

    private fun productAlreadyAdded(holder: AutocompleteElementsViewHolder) {
        holder.ivAutocompleteElementAddedStatus.visibility = View.VISIBLE
        holder.tvAutocompleteElementCanAddStatus.visibility = View.GONE
    }

    private fun productCanBeAdded(holder: AutocompleteElementsViewHolder, position: Int) {
        holder.ivAutocompleteElementAddedStatus.visibility = View.GONE
        holder.tvAutocompleteElementCanAddStatus.visibility = View.VISIBLE

        holder.llAutocompleteElement.setOnClickListener {
            onAddElement?.invoke(products[position])
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }
}