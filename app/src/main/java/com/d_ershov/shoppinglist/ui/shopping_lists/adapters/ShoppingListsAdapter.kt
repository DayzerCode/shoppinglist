package com.d_ershov.shoppinglist.ui.shopping_lists.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.dto.ShoppingListWithNumberProductsDto

class ShoppingListsAdapter(
    private var shoppingLists: List<ShoppingListWithNumberProductsDto>
) : RecyclerView.Adapter<ShoppingListsViewHolder>() {
    var onDelete : ((ShoppingListWithNumberProductsDto) -> Unit)? = null
    var onNavigateToDetail : ((ShoppingListWithNumberProductsDto) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShoppingListsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.shopping_lists, parent, false)
        return ShoppingListsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ShoppingListsViewHolder, position: Int) {
        holder.tvName.text = shoppingLists[position].name
        holder.tvNumberItems.text = shoppingLists[position].numberProducts.toString()
        holder.tvNumberPurchasedItems.text = shoppingLists[position].numberPurchasedProducts.toString()
        holder.ivDelete.setOnClickListener {
            onDelete?.invoke(shoppingLists[position])
        }

        holder.llShoppingListElement.setOnClickListener {
            onNavigateToDetail?.invoke(shoppingLists[position])
        }
    }

    override fun getItemCount(): Int {
        return shoppingLists.size
    }
}