package com.d_ershov.shoppinglist.ui.shopping_detail.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.db.dto.ShoppingListProductsDto

class ShoppingListDetailElementsAdapter(
    private var products: MutableList<ShoppingListProductsDto>
) : RecyclerView.Adapter<ShoppingListDetailElementsViewHolder>() {
    var onDeleteElement: ((ShoppingListProductsDto) -> Unit)? = null
    var onSelectElement: ((ShoppingListProductsDto, Boolean) -> Unit)? = null
    var onChangeCount: ((Long) -> Unit)? = null
    private val categoryPositions = mutableListOf<Int>()

    init {
        calculateCategoryPositions()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShoppingListDetailElementsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View =
            layoutInflater.inflate(R.layout.shopping_list_element, parent, false)
        return ShoppingListDetailElementsViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ShoppingListDetailElementsViewHolder, position: Int) {
        if (needShowCategoryName(position)) {
            holder.tvCategory.text = products[position].categoryName
            holder.tvCategory.visibility = View.VISIBLE
        } else {
            holder.tvCategory.text = products[position].categoryName
            holder.tvCategory.visibility = View.GONE
        }

        holder.tvShoppingListElement.text = products[position].productName
        holder.tvShoppingListElementCount.text = products[position].count.toString()
        holder.rbShoppingDetailElementBought.isChecked = products[position].bought

        holder.ivShoppingListElementDelete.setOnClickListener {
            onDeleteElement?.invoke(products[position])
            products.removeAt(position)
            calculateCategoryPositions()
            notifyDataSetChanged()
        }

        holder.rbShoppingDetailElementBought.setOnClickListener { view ->
            val checkbox = view as CheckBox
            onSelectElement?.invoke(products[position], checkbox.isChecked)
        }

        holder.tvClickChangeCount.setOnClickListener {
            onChangeCount?.invoke(products[position].productId)
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }

    private fun needShowCategoryName(position: Int): Boolean {
        return categoryPositions.contains(position)
    }

    private fun calculateCategoryPositions() {
        categoryPositions.clear()
        var currentCategoryName = ""
        products.forEachIndexed { index, product ->
            if (product.categoryName != currentCategoryName) {
                categoryPositions.add(index)
                currentCategoryName = product.categoryName
            }
        }
        Log.d("categoryPositions", categoryPositions.toString())
    }
}