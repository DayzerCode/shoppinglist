package com.d_ershov.shoppinglist.ui.import_product

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.d_ershov.shoppinglist.import_products.ImportManager
import com.d_ershov.shoppinglist.states.state_import.StateImport
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ImportProductViewModel() : ViewModel() {
    @Inject
    lateinit var importManager: ImportManager
    val state = MutableLiveData(StateImport.loading)

    fun loadState() {
        CoroutineScope(Dispatchers.IO).launch {
            if (importManager.canImport()) {
                withContext(Dispatchers.Main) {
                    state.value = StateImport.noProducts
                }
            } else {
                withContext(Dispatchers.Main) {
                    state.value = StateImport.success
                }
            }
        }
    }

    fun import() {
        CoroutineScope(Dispatchers.IO).launch {
            var resultState: StateImport = StateImport.error
            try {
                if (importManager.import()) {
                    resultState = StateImport.success
                }
            } catch (e: Exception) {
                if (e.message != null) {
                    Log.d("Import Product Error", e.message!!)
                }
            }
            withContext(Dispatchers.Main) {
                state.value = resultState
            }
        }
    }
}