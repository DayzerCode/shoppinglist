package com.d_ershov.shoppinglist

import android.app.Application
import com.d_ershov.shoppinglist.di.AppComponent
import com.d_ershov.shoppinglist.di.DaggerAppComponent

class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .context(this)
            .build()
    }
}