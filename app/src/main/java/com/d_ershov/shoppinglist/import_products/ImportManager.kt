package com.d_ershov.shoppinglist.import_products

import android.content.SharedPreferences
import android.content.res.AssetManager
import android.content.res.Resources
import android.util.Log
import com.d_ershov.shoppinglist.R
import com.d_ershov.shoppinglist.constants.SharedPreferencesConstants
import com.d_ershov.shoppinglist.db.entities.CategoryEntity
import com.d_ershov.shoppinglist.db.entities.ProductEntity
import com.d_ershov.shoppinglist.repositories.CategoryRepository
import com.d_ershov.shoppinglist.repositories.ProductsRepository
import java.io.BufferedReader

class ImportManager(
    private val categoryRepository: CategoryRepository,
    private val productsRepository: ProductsRepository,
    private val assetManager: AssetManager,
    private var sharedPreferences: SharedPreferences,
    private var resources: Resources
) {
    suspend fun import(): Boolean {
        val categories = arrayListOf(
            CategoryEntity(resources.getString(R.string.import_vegetable), "vegetable", 10),
            CategoryEntity(resources.getString(R.string.import_fruits), "fruits", 20),
            CategoryEntity(resources.getString(R.string.import_berries), "berries", 30),
            CategoryEntity(resources.getString(R.string.import_nuts), "nuts", 40),
            CategoryEntity(resources.getString(R.string.import_bean), "bean", 50),
            CategoryEntity(resources.getString(R.string.import_cereals), "cereals", 60),
            CategoryEntity(resources.getString(R.string.import_milky), "milky", 70),
            CategoryEntity(resources.getString(R.string.import_mushrooms), "mushrooms", 80),
            CategoryEntity(resources.getString(R.string.import_wheat_products), "wheat_products", 90),
            CategoryEntity(resources.getString(R.string.import_meat), "meat", 100),
            CategoryEntity(resources.getString(R.string.import_fish), "fish", 110),
            CategoryEntity(resources.getString(R.string.import_sweet), "sweet", 120)
        )
        categoryRepository.add(categories)
        val categoriesFromDb = categoryRepository.getAll()
        for (category in categoriesFromDb) {
            if (category.categoryId == null) {
                continue
            }
            assetManager
            try {
                val reader = BufferedReader(assetManager.open(category.code + ".txt").reader())
                try {
                    var productName: String?
                    do {
                        productName = reader.readLine()
                        if (productName == null)
                            break
                        productsRepository.add(ProductEntity(productName, category.categoryId!!))
                    } while (true)
                } catch (e: Exception) {
                    if (e.message != null) {
                        Log.d("Import Product Error", e.message!!)
                        return false
                    }
                } finally {
                    reader.close()
                }
            } catch (e: Exception) {
                Log.d("Import Product Error", e.message!!)
                return false
            }
        }
        sharedPreferences.edit()
            .putBoolean(SharedPreferencesConstants.OPTION_IMPORT_MADE_NAME, true).apply()
        return true
    }

    suspend fun canImport(): Boolean {
        var name = SharedPreferencesConstants.OPTION_IMPORT_MADE_NAME
        return sharedPreferences.getBoolean(name, false) || productsRepository.count() == 0
    }
}