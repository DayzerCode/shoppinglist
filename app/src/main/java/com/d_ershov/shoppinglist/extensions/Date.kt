package com.d_ershov.shoppinglist.extensions

import android.annotation.SuppressLint
import com.d_ershov.shoppinglist.constants.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.getCurrentAsString() : String {
    val formatter = SimpleDateFormat(DateFormat.FORMAT_DATE_TIME)
    return formatter.format(this)
}