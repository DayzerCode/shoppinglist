package com.d_ershov.shoppinglist.di

import android.content.SharedPreferences
import android.content.res.AssetManager
import android.content.res.Resources
import com.d_ershov.shoppinglist.import_products.ImportManager
import com.d_ershov.shoppinglist.repositories.CategoryRepository
import com.d_ershov.shoppinglist.repositories.ProductsRepository
import dagger.Module
import dagger.Provides

@Module
class ImportModule {
    @Provides
    fun provideImportManager(
        categoryRepository: CategoryRepository,
        productsRepository: ProductsRepository,
        assetManager: AssetManager,
        sharedPreferences: SharedPreferences,
        resources: Resources
    ): ImportManager {
        return ImportManager(
            categoryRepository,
            productsRepository,
            assetManager,
            sharedPreferences,
            resources
        )
    }
}