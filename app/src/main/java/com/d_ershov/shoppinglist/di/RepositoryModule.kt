package com.d_ershov.shoppinglist.di

import android.content.Context
import com.d_ershov.shoppinglist.db.Database
import com.d_ershov.shoppinglist.repositories.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideDatabase(context: Context): Database {
        return Database.getInstance(context)
    }

    @Provides
    fun provideShoppingListRepository(database: Database): ShoppingListRepository {
        return ShoppingListRepository(database.shoppingListDao)
    }

    @Provides
    fun provideShoppingListProductsRepository(database: Database): ShoppingListProductsRepository {
        return ShoppingListProductsRepository(database.shoppingListProductsDao)
    }

    @Provides
    fun provideProductsRepository(database: Database): ProductsRepository {
        return ProductsRepository(database.productDao)
    }

    @Provides
    fun provideCategoryRepository(database: Database): CategoryRepository {
        return CategoryRepository(database.categoryDao)
    }

    @Provides
    fun providePreparedListRepository(database: Database): PreparedListRepository {
        return PreparedListRepository(database.preparedListDao)
    }

    @Provides
    fun providePreparedListProductsRepository(database: Database): PreparedListProductsRepository {
        return PreparedListProductsRepository(database.preparedListProductsDao)
    }
}