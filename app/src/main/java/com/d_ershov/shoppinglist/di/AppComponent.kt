package com.d_ershov.shoppinglist.di

import android.content.Context
import com.d_ershov.shoppinglist.ui.import_product.ImportProductFragment
import com.d_ershov.shoppinglist.ui.import_product.ImportProductViewModel
import com.d_ershov.shoppinglist.ui.prepared_list_detail.PreparedListDetailFragment
import com.d_ershov.shoppinglist.ui.prepared_list_detail.PreparedListDetailViewModel
import com.d_ershov.shoppinglist.ui.prepared_lists.PreparedListsFragment
import com.d_ershov.shoppinglist.ui.prepared_lists.PreparedListsViewModel
import com.d_ershov.shoppinglist.ui.rating.RatingViewModel
import com.d_ershov.shoppinglist.ui.shopping_detail.ShoppingDetailFragment
import com.d_ershov.shoppinglist.ui.shopping_detail.ShoppingDetailViewModel
import com.d_ershov.shoppinglist.ui.shopping_lists.ShoppingListsFragment
import com.d_ershov.shoppinglist.ui.shopping_lists.ShoppingListsViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [RepositoryModule::class, ContextModule::class, SharedPreferencesModule::class, ImportModule::class])
@Singleton
interface AppComponent {
    fun inject(fragment: ShoppingDetailFragment)
    fun inject(fragment: ShoppingListsFragment)
    fun inject(fragment: ImportProductFragment)
    fun inject(fragment: PreparedListsFragment)
    fun inject(fragment: PreparedListDetailFragment)


    fun inject(viewModel: PreparedListsViewModel)
    fun inject(viewModel: ImportProductViewModel)
    fun inject(viewModel: PreparedListDetailViewModel)
    fun inject(viewModel: ShoppingDetailViewModel)
    fun inject(viewModel: ShoppingListsViewModel)
    fun inject(viewModel: RatingViewModel)
    fun context(): Context
    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
    }
}