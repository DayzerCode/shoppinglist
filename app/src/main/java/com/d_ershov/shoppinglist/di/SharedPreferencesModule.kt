package com.d_ershov.shoppinglist.di

import android.content.Context
import android.content.SharedPreferences
import com.d_ershov.shoppinglist.constants.SharedPreferencesConstants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPreferencesModule {
    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(SharedPreferencesConstants.PREFERENCE_NAME, Context.MODE_PRIVATE)
    }
}