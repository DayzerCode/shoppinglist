package com.d_ershov.shoppinglist.di

import android.content.Context
import android.content.res.AssetManager
import android.content.res.Resources
import dagger.Module
import dagger.Provides

@Module
class ContextModule {
    @Provides
    fun provideAssetManager(context: Context): AssetManager {
        return context.assets
    }

    @Provides
    fun provideResources(context: Context): Resources {
        return context.resources
    }
}