package com.d_ershov.shoppinglist.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "prepared_list_products",
    primaryKeys = ["preparedListId", "productId"],
    foreignKeys = [
        ForeignKey(
            entity = PreparedListEntity::class,
            parentColumns = ["preparedListId"],
            childColumns = ["preparedListId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ProductEntity::class,
            parentColumns = ["productId"],
            childColumns = ["productId"],
            onDelete = ForeignKey.CASCADE
        )]
)
data class PreparedListProductsEntity(
    var preparedListId: Long,
    var productId: Long
)