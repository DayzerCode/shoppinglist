package com.d_ershov.shoppinglist.db.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParcelableCategoryDto(var categoryId: Int, var name: String): Parcelable