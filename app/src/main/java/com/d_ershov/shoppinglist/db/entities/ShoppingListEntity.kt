package com.d_ershov.shoppinglist.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shopping_lists")
data class ShoppingListEntity(
    var name: String
) {
    @PrimaryKey(autoGenerate = true)
    var shoppingListId: Long? = null
}