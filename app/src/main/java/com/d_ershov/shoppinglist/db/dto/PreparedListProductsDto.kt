package com.d_ershov.shoppinglist.db.dto

data class PreparedListProductsDto(
    var productId: Long,
    var productName: String

)