package com.d_ershov.shoppinglist.db.dto

data class ShoppingListWithNumberProductsDto(
    var shoppingListId: Long,
    var name: String,
    var numberProducts: Int = 0,
    var numberPurchasedProducts: Int = 0
)