package com.d_ershov.shoppinglist.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(tableName = "shopping_list_products",
    primaryKeys = ["shoppingListId", "productId"],
    foreignKeys = [
        ForeignKey(
            entity = ShoppingListEntity::class,
            parentColumns = ["shoppingListId"],
            childColumns = ["shoppingListId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ProductEntity::class,
            parentColumns = ["productId"],
            childColumns = ["productId"],
            onDelete = ForeignKey.CASCADE
        )])
data class ShoppingListProductsEntity (
    val shoppingListId: Long,
    val productId: Long,
    var bought: Boolean = false,
    var count: Short = 1
)