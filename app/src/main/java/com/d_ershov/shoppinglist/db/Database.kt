package com.d_ershov.shoppinglist.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.d_ershov.shoppinglist.db.dao.*
import com.d_ershov.shoppinglist.db.entities.*

@Database(
    entities = [CategoryEntity::class, ProductEntity::class, ShoppingListEntity::class, ShoppingListProductsEntity::class, PreparedListEntity::class, PreparedListProductsEntity::class],
    version = 1,
    exportSchema = false
)
abstract class Database : RoomDatabase() {
    abstract val categoryDao: CategoryDao
    abstract val productDao: ProductDao
    abstract val shoppingListDao: ShoppingListDao
    abstract val shoppingListProductsDao: ShoppingListProductsDao
    abstract val preparedListDao: PreparedListDao
    abstract val preparedListProductsDao: PreparedListProductsDao

    companion object {
        @Volatile
        private var instance: com.d_ershov.shoppinglist.db.Database? = null
        fun getInstance(context: Context): com.d_ershov.shoppinglist.db.Database {
            synchronized(this) {
                var instance = this.instance
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        com.d_ershov.shoppinglist.db.Database::class.java,
                        "shopping_list"
                    ).fallbackToDestructiveMigration().build()
                }
                return instance
            }
        }
    }
}