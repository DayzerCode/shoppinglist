package com.d_ershov.shoppinglist.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.d_ershov.shoppinglist.db.dto.ShoppingListProductsDto
import com.d_ershov.shoppinglist.db.entities.ShoppingListProductsEntity

@Dao
interface ShoppingListProductsDao {
    @Query("SELECT p.name as productName, p.productId AS productId, slp.shoppingListId AS shoppingListId, bought, count, c.name as categoryName FROM shopping_list_products AS slp LEFT JOIN products AS p ON slp.productId = p.productId LEFT JOIN categories as c ON p.categoryOwnerId = c.categoryId  WHERE shoppingListId = :id ORDER BY c.sort")
    fun getByShoppingListId(id: Long): LiveData<List<ShoppingListProductsDto>>

    @Query("SELECT p.name as productName, p.productId AS productId, slp.shoppingListId AS shoppingListId, bought, count, c.name as categoryName FROM shopping_list_products AS slp LEFT JOIN products AS p ON slp.productId = p.productId LEFT JOIN categories as c ON p.categoryOwnerId = c.categoryId  WHERE shoppingListId = :id AND bought = :isPurchased ORDER BY c.sort")
    fun getByShoppingListId(id: Long, isPurchased: Boolean): LiveData<List<ShoppingListProductsDto>>

    @Insert()
    suspend fun add(shoppingListProductsEntity: ShoppingListProductsEntity)

    @Update()
    suspend fun update(shoppingListProductsEntity: ShoppingListProductsEntity)
    @Query("UPDATE shopping_list_products SET count = :count WHERE productId = :productId AND shoppingListId = :shoppingListId")
    suspend fun updateCount(productId : Long, shoppingListId : Long, count : Short)
    @Query("DELETE FROM shopping_list_products WHERE productId = :productId AND shoppingListId = :shoppingListId")
    suspend fun delete(productId: Long, shoppingListId: Long)

}