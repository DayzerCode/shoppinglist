package com.d_ershov.shoppinglist.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "products", foreignKeys = [
    ForeignKey(
        entity = CategoryEntity::class,
        parentColumns = ["categoryId"],
        childColumns = ["categoryOwnerId"],
        onDelete = ForeignKey.CASCADE
    )])
data class ProductEntity(
    var name: String,
    var categoryOwnerId: Long
) {
    @PrimaryKey(autoGenerate = true)
    var productId: Long? = null
    var numberAdditions: Long = 0
}