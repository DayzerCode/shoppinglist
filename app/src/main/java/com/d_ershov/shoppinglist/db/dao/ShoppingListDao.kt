package com.d_ershov.shoppinglist.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.d_ershov.shoppinglist.db.dto.ShoppingListWithNumberProductsDto
import com.d_ershov.shoppinglist.db.entities.ShoppingListEntity

@Dao
interface ShoppingListDao {
    @Query("SELECT * FROM shopping_lists ORDER BY shoppingListId DESC")
    fun findAll(): LiveData<List<ShoppingListEntity>>

    @Query("SELECT name, COUNT(slp.productId) AS numberProducts, COUNT(CASE WHEN slp.bought=1 THEN 1 ELSE NULL END) AS numberPurchasedProducts, sl.shoppingListId AS shoppingListId FROM shopping_lists AS sl LEFT JOIN shopping_list_products AS slp ON sl.shoppingListId = slp.shoppingListId GROUP BY sl.shoppingListId ORDER BY sl.shoppingListId DESC")
    fun findAllWithNumberProducts(): LiveData<List<ShoppingListWithNumberProductsDto>>

    @Query("SELECT * FROM shopping_lists WHERE shoppingListId = :id")
    fun findById(id: Long): ShoppingListEntity?

    @Query("SELECT * FROM shopping_lists WHERE shoppingListId = :id")
    fun getById(id: Long): LiveData<ShoppingListEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(shoppingLists: List<ShoppingListEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(shoppingList: ShoppingListEntity) : Long

    @Update
    suspend fun update(shoppingList: ShoppingListEntity)

    @Delete
    suspend fun delete(shoppingList: ShoppingListEntity)

    @Query("DELETE FROM shopping_lists WHERE shoppingListId = :shoppingListId")
    suspend fun delete(shoppingListId: Long)

    @Query("DELETE FROM shopping_list_products WHERE shoppingListId = :shoppingListId")
    suspend fun deleteProductsFromShoppingList(shoppingListId: Long)
}