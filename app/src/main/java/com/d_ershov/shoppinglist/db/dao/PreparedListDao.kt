package com.d_ershov.shoppinglist.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity

@Dao
interface PreparedListDao {
    @Query("SELECT * FROM prepared_list")
    fun findAll(): LiveData<List<PreparedListEntity>>
    @Query("SELECT * FROM prepared_list")
    fun getAll(): List<PreparedListEntity>
    @Query("SELECT * FROM prepared_list WHERE preparedListId = :id")
    fun findById(id: Long): LiveData<PreparedListEntity>
    @Query("SELECT * FROM prepared_list WHERE preparedListId = :id")
    fun getById(id: Long): PreparedListEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(preparedList: PreparedListEntity) : Long

    @Update
    suspend fun update(preparedList: PreparedListEntity)

    @Delete
    suspend fun delete(preparedList: PreparedListEntity)

    @Query("DELETE FROM prepared_list WHERE preparedListId = :preparedListId")
    suspend fun delete(preparedListId: Long)


}