package com.d_ershov.shoppinglist.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "prepared_list")
data class PreparedListEntity(
    var name: String,
    var sort: Int = 50
) {
    @PrimaryKey(autoGenerate = true)
    var preparedListId: Long? = null

    override fun toString(): String {
        return name
    }
}