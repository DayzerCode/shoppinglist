package com.d_ershov.shoppinglist.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.d_ershov.shoppinglist.db.entities.ProductEntity

@Dao
interface ProductDao {
    @Query("SELECT * FROM products")
    fun findAll(): LiveData<List<ProductEntity>>
    @Query("SELECT * FROM products WHERE numberAdditions > 0 ORDER BY numberAdditions DESC")
    fun findAllForRating(): LiveData<List<ProductEntity>>
    @Query("SELECT * FROM products WHERE productId = :id")
    suspend fun getById(id: Long): ProductEntity?

    @Query("SELECT p.productId AS productId, p.categoryOwnerId AS categoryOwnerId, p.name AS name, p.numberAdditions AS numberAdditions FROM products AS p LEFT JOIN shopping_list_products ON shopping_list_products.productId = p.productId  WHERE name LIKE :name ORDER BY shopping_list_products.bought, numberAdditions DESC")
    suspend fun searchByName(name: String): List<ProductEntity>
    @Query("SELECT COUNT(productId) FROM products")
    suspend fun count(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(products: List<ProductEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(product: ProductEntity) : Long

    @Update
    suspend fun update(product: ProductEntity)

    @Delete
    suspend fun delete(product: ProductEntity)


}