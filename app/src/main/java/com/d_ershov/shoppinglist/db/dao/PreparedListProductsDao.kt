package com.d_ershov.shoppinglist.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.d_ershov.shoppinglist.db.dto.PreparedListProductsDto
import com.d_ershov.shoppinglist.db.entities.PreparedListProductsEntity

@Dao
interface PreparedListProductsDao {
    @Query("SELECT pl.productId AS productId, p.name AS productName FROM prepared_list_products AS pl LEFT JOIN products AS p ON pl.productId = p.productId WHERE preparedListId = :id")
    fun findProductsByPreparedList(id: Long): LiveData<List<PreparedListProductsDto>>

    @Query("SELECT pl.productId AS productId, p.name AS productName FROM prepared_list_products AS pl LEFT JOIN products AS p ON pl.productId = p.productId WHERE preparedListId = :id")
    fun getProductsByPreparedList(id: Long): List<PreparedListProductsDto>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(preparedListProductsEntity: PreparedListProductsEntity)

    @Update
    suspend fun update(preparedListProductsEntity: PreparedListProductsEntity)

    @Delete
    suspend fun delete(preparedListProductsEntity: PreparedListProductsEntity)

    @Query("DELETE FROM prepared_list_products WHERE preparedListId = :preparedListId")
    suspend fun deleteByPreparedListId(preparedListId: Long)
}