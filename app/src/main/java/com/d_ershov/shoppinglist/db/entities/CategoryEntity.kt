package com.d_ershov.shoppinglist.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categories")
data class CategoryEntity(
    var name: String,
    var code: String,
    var sort: Int = 50
) {
    @PrimaryKey(autoGenerate = true)
    var categoryId: Long? = null

    override fun toString(): String {
        return name
    }
}