package com.d_ershov.shoppinglist.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.d_ershov.shoppinglist.db.dto.ParcelableCategoryDto
import com.d_ershov.shoppinglist.db.entities.CategoryEntity

@Dao
interface CategoryDao {
    @Query("SELECT * FROM categories")
    suspend fun getAll(): List<CategoryEntity>

    @Query("SELECT * FROM categories")
    fun findAll(): LiveData<List<CategoryEntity>>

    @Query("SELECT * FROM categories")
    fun parcelableFindAll(): List<ParcelableCategoryDto>

    @Query("SELECT * FROM categories WHERE categoryId = :id")
    fun findById(id: Long): CategoryEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(categories: List<CategoryEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(category: CategoryEntity)

    @Update
    suspend fun update(category: CategoryEntity)

    @Delete
    suspend fun delete(category: CategoryEntity)
}