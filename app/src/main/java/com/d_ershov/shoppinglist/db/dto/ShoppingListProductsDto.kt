package com.d_ershov.shoppinglist.db.dto

data class ShoppingListProductsDto (
    val productId: Long,
    val shoppingListId: Long,
    val productName: String,
    var bought: Boolean,
    var count: Short,
    var categoryName: String
)