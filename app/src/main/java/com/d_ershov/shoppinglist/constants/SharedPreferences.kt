package com.d_ershov.shoppinglist.constants

object SharedPreferencesConstants{
    val PREFERENCE_NAME = "settings"
    val OPTION_IMPORT_MADE_NAME = "import_made"
}