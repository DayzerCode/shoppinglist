package com.d_ershov.shoppinglist.constants

object DateFormat {
    val FORMAT_DATE_TIME ="dd.MM.yyyy HH:mm"
}