package com.d_ershov.shoppinglist.states.state_import

class StateImport private constructor(val status: StateImportEnum, val msg: String? = null) {
    companion object {
        val loading = StateImport(
            StateImportEnum.LOADING
        )
        val noProducts = StateImport(
            StateImportEnum.NO_PRODUCTS
        )
        val success = StateImport(
            StateImportEnum.SUCCESS
        )
        val error = StateImport(
            StateImportEnum.ERROR
        )
    }
}