package com.d_ershov.shoppinglist.states.default_state

enum class StateEnum {
    LOADING,
    READY,
    ERROR
}