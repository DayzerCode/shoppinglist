package com.d_ershov.shoppinglist.states.state_import

enum class StateImportEnum {
    LOADING,
    NO_PRODUCTS,
    SUCCESS,
    ERROR,
}