package com.d_ershov.shoppinglist.states.default_state

class State private constructor(val status: StateEnum, val msg: String? = null) {
    companion object {
        val loading = State(
            StateEnum.LOADING
        )
        val ready = State(
            StateEnum.READY
        )
    }
}