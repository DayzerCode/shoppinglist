package com.d_ershov.shoppinglist.repositories

import androidx.lifecycle.LiveData
import com.d_ershov.shoppinglist.db.dao.CategoryDao
import com.d_ershov.shoppinglist.db.entities.CategoryEntity

class CategoryRepository(private val dao: CategoryDao) {

    suspend fun add(categories : List<CategoryEntity>) {
        dao.add(categories)
    }

    suspend fun getAll(): List<CategoryEntity> {
        return dao.getAll()
    }

    fun findAll(): LiveData<List<CategoryEntity>> {
        return dao.findAll()
    }
}