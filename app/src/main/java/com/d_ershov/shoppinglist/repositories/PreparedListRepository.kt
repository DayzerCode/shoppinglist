package com.d_ershov.shoppinglist.repositories

import androidx.lifecycle.LiveData
import com.d_ershov.shoppinglist.db.dao.PreparedListDao
import com.d_ershov.shoppinglist.db.entities.PreparedListEntity

class PreparedListRepository(private val dao: PreparedListDao) {
    fun findAll() : LiveData<List<PreparedListEntity>> {
        return dao.findAll()
    }

    suspend fun getAll() : List<PreparedListEntity> {
        return dao.getAll()
    }

    fun findById(id: Long) : LiveData<PreparedListEntity> {
        return dao.findById(id)
    }

    suspend fun add(preparedList: PreparedListEntity) : Long {
        return dao.add(preparedList)
    }

    suspend fun new(name: String) : Long {
        return dao.add(PreparedListEntity(name))
    }

    suspend fun update(preparedList: PreparedListEntity) {
        dao.update(preparedList)
    }

    suspend fun delete(preparedList: PreparedListEntity) {
        dao.delete(preparedList)
    }
    suspend fun delete(preparedListId: Long) {
        dao.delete(preparedListId)
    }


}