package com.d_ershov.shoppinglist.repositories

import androidx.lifecycle.LiveData
import com.d_ershov.shoppinglist.db.dao.PreparedListProductsDao
import com.d_ershov.shoppinglist.db.dto.PreparedListProductsDto
import com.d_ershov.shoppinglist.db.entities.PreparedListProductsEntity

class PreparedListProductsRepository(private val dao: PreparedListProductsDao) {

    fun findProductsByPreparedList(preparedListId: Long): LiveData<List<PreparedListProductsDto>> {
        return dao.findProductsByPreparedList(preparedListId)
    }

    suspend fun getProductsByPreparedList(preparedListId: Long): List<PreparedListProductsDto> {
        return dao.getProductsByPreparedList(preparedListId)
    }

    suspend fun add(preparedListProducts: PreparedListProductsEntity) {
        dao.add(preparedListProducts)
    }

    suspend fun add(preparedListId: Long, productId: Long) {
        add(PreparedListProductsEntity(preparedListId, productId))
    }

    suspend fun update(preparedListProducts: PreparedListProductsEntity) {
        dao.update(preparedListProducts)
    }

    suspend fun delete(preparedListProducts: PreparedListProductsEntity) {
        dao.delete(preparedListProducts)
    }

    suspend fun delete(preparedListId: Long, productId: Long) {
        delete(PreparedListProductsEntity(preparedListId, productId))
    }

    suspend fun deleteByPreparedListId(preparedListId: Long) {
        dao.deleteByPreparedListId(preparedListId)
    }
}