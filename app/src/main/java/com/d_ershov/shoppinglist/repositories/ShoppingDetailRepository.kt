package com.d_ershov.shoppinglist.repositories

class ShoppingDetailRepository(val shoppingList: ShoppingListRepository, val shoppingListProducts: ShoppingListProductsRepository, val product: ProductsRepository, val category: CategoryRepository)