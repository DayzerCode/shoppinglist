package com.d_ershov.shoppinglist.repositories

import androidx.lifecycle.LiveData
import com.d_ershov.shoppinglist.db.dao.ShoppingListProductsDao
import com.d_ershov.shoppinglist.db.dto.ShoppingListProductsDto
import com.d_ershov.shoppinglist.db.entities.ShoppingListProductsEntity

class ShoppingListProductsRepository(private val dao: ShoppingListProductsDao) {
    fun getProducts(shoppingListId: Long, needFilter: Boolean = false) : LiveData<List<ShoppingListProductsDto>> {
        if (needFilter) {
            return dao.getByShoppingListId(shoppingListId, false)
        }
        return dao.getByShoppingListId(shoppingListId)
    }

    suspend fun delete(productId : Long, shoppingListId: Long) {
        dao.delete(productId, shoppingListId)
    }

    suspend fun update(element: ShoppingListProductsDto) {
        val entity = ShoppingListProductsEntity(element.shoppingListId, element.productId, element.bought, element.count)
        dao.update(entity)
    }

    suspend fun updateCount(productId: Long, shoppingListId: Long, count: Short ) {
        dao.updateCount(productId, shoppingListId, count)
    }

    suspend fun addInShoppingList(productId: Long, currentShoppingListId: Long) {
        val shoppingListProduct = ShoppingListProductsEntity(currentShoppingListId, productId)
        dao.add(shoppingListProduct)
    }
}