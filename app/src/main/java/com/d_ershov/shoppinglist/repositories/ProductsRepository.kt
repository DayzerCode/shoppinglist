package com.d_ershov.shoppinglist.repositories

import androidx.lifecycle.LiveData
import com.d_ershov.shoppinglist.db.dao.ProductDao
import com.d_ershov.shoppinglist.db.entities.ProductEntity

class ProductsRepository(private val dao: ProductDao) {

    fun findAll() : LiveData<List<ProductEntity>> {
        return dao.findAll()
    }

    suspend fun getById(id: Long) : ProductEntity? {
        return dao.getById(id)
    }

    suspend fun searchByName(query: String) : List<ProductEntity> {
        return dao.searchByName(query)
    }

    suspend fun increaseCounterAdditions(product: ProductEntity) {
        product.numberAdditions += 1
        dao.update(product)
    }

    suspend fun add(productEntity: ProductEntity) : Long {
        return dao.add(productEntity)
    }

    suspend fun count() : Int {
        return dao.count()
    }

    fun findAllForRating(): LiveData<List<ProductEntity>> {
        return dao.findAllForRating()
    }
}