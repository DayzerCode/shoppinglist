package com.d_ershov.shoppinglist.repositories

import androidx.lifecycle.LiveData
import com.d_ershov.shoppinglist.db.dao.ShoppingListDao
import com.d_ershov.shoppinglist.db.dto.ShoppingListWithNumberProductsDto
import com.d_ershov.shoppinglist.db.entities.ShoppingListEntity

class ShoppingListRepository(private val dao: ShoppingListDao) {
    fun getById(shoppingListId: Long) : LiveData<ShoppingListEntity> {
        return dao.getById(shoppingListId)
    }

    suspend fun new(name: String) : Long {
        return dao.add(ShoppingListEntity(name))
    }

    fun getShoppingListsWithNumberProducts(): LiveData<List<ShoppingListWithNumberProductsDto>> {
        return dao.findAllWithNumberProducts()
    }

    suspend fun delete(shoppingListEntity : ShoppingListEntity) {
        dao.deleteProductsFromShoppingList(shoppingListEntity.shoppingListId!!)
        dao.delete(shoppingListEntity)
    }

    suspend fun delete(shoppingListId : Long) {
        dao.deleteProductsFromShoppingList(shoppingListId)
        dao.delete(shoppingListId)
    }
}